import tensorflow

device_name = tensorflow.test.gpu_device_name()
if device_name != '/device:GPU:0':
    import tensorflow as tf
else:
    with tensorflow.device('/device:GPU:0'):
        import tensorflow as tf


class DenseNet169_Base:
    def __init__(self):
        '''
        image_shape - Standard image shape as input shape for DenseNet model.
        DenseNet169 - Base DenseNet169 model, pre-trained on ImageNet dataset.
        '''
        self.image_shape = (224, 224, 3)
        self.densenet169 = tf.keras.applications.DenseNet169(weights='imagenet', include_top=False,
                                                             input_shape=self.image_shape, pooling='avg')

    def create_model(self):
        '''
        Create a DenseNet169 model based on ImageNet dataset, standard input layer to get images with shape
        (224,224,3), modified last layers - Dense with Sigmoid activation function, to predict the probability of a
        class (1 or 0).
        :return:
        modelDenseNet169 - based DenseNet169 model.
        '''
        modeldensenet169 = tf.keras.models.Sequential(
            [self.densenet169, tf.keras.layers.Flatten(), tf.keras.layers.Dense(1, activation='sigmoid')])
        modeldensenet169.compile(loss=tf.keras.losses.binary_crossentropy,
                                 optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.9, beta_2=0.999),
                                 metrics=['accuracy'])
        return modeldensenet169
