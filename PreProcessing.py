import tensorflow
import pandas as pd
from pathlib import Path

device_name = tensorflow.test.gpu_device_name()
if device_name != '/device:GPU:0':
    import tensorflow as tf
else:
    with tensorflow.device('/device:GPU:0'):
        import tensorflow as tf


class PreProcessing:
    def __init__(self, data_directory):
        '''
        data_directory - Directory of the dataset.
        dataset_dir - Path to the directory of the dataset.
        bones_category - List of categories from the dataset that we will work with them in our model.
        batch_size - batch size we using in the generator of the dataset.
        '''
        try:
            self.dataset_dir = Path(data_directory)
        except:
            raise Exception("Failed to load data directory path.")
        self.bones_category = ["HUMERUS", "FOREARM"]
        self.batch_size = 8

    def generate_df(self, csv_file):
        '''
        Craete a dataframe of the labels and the dataset.
        :param:
        csv_file - csv file that contains the dataset and the labels. 
        :return:
        df - Dataframe of the labels and the dataset.
        '''
        df = pd.read_csv(self.dataset_dir / csv_file, header=None, names=['filename'])
        df = df[df.filename.str.contains('|'.join(self.bones_category))]
        df.reset_index(inplace=True, drop=True)
        df['class'] = (df.filename.str.extract('study.*_(positive|negative)'))
        return df

    def train_generator(self, model_type):
        '''
        Create a train generator to get images with shape (224, 224) or (320, 320) depends on the model type.
        
        :param:
        model_type - String of the model type, can be "base" or "finetune".
        :return:
        train_generator - Generator of the train dataset.        
        '''
        if "base" in model_type:
            train_datagenerator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
            train_generator = train_datagenerator.flow_from_dataframe(self.generate_df('train_image_paths.csv'),
                                                                      directory=self.dataset_dir.parent,
                                                                      target_size=(224, 224),
                                                                      batch_size=self.batch_size,
                                                                      class_mode='binary')
        elif "finetune" in model_type:
            train_datagenerator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255, rotation_range=30,
                                                                                  horizontal_flip=True)
            train_generator = train_datagenerator.flow_from_dataframe(self.generate_df('train_image_paths.csv'),
                                                                      directory=self.dataset_dir.parent,
                                                                      target_size=(320, 320),
                                                                      batch_size=self.batch_size,
                                                                      class_mode='binary')
        return train_generator

    def valid_generator(self, model_type):
        '''
        Create a validation generator to get images with shape (224, 224) or (320, 320) depends on the model type.

        :param:
        model_type - String of the model type, can be "base" or "finetune".
        :return:
        valid_generator - Generator of the validation dataset.        
        '''
        valid_datagenerator = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
        if "base" in model_type:
            valid_generator = valid_datagenerator.flow_from_dataframe(self.generate_df('valid_image_paths.csv'),
                                                                      directory=self.dataset_dir.parent,
                                                                      target_size=(224, 224),
                                                                      batch_size=self.batch_size,
                                                                      class_mode='binary')
        elif "finetune" in model_type:
            valid_generator = valid_datagenerator.flow_from_dataframe(self.generate_df('valid_image_paths.csv'),
                                                                      directory=self.dataset_dir.parent,
                                                                      target_size=(320, 320),
                                                                      batch_size=self.batch_size,
                                                                      class_mode='binary')
        return valid_generator
