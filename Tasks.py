import tensorflow
import matplotlib.pyplot as plt
from DenseNet169_Base import DenseNet169_Base
from DenseNet169_FineTune import DenseNet169_FineTune
from PreProcessing import PreProcessing

device_name = tensorflow.test.gpu_device_name()
if device_name != '/device:GPU:0':
    import tensorflow as tf
else:
    with tensorflow.device('/device:GPU:0'):
        import tensorflow as tf


def plot(base_model_results, finetune_model_results):
    '''
    Plots Accuracy and Loss graphs of the validation set of the models.

    :param:
    base_model_results - Base DenseNet169 model that has been trained.
    finetune_model_results - Finetuned DenseNet169 model that has been trained.
    '''
    figure = plt.figure(figsize=(15, 15))
    plt.subplot(211)
    plt.title('Test Accuracy')
    plt.plot(base_model_results.history['val_accuracy'], label='Base DenseNet169 Accuracy')
    plt.plot(finetune_model_results.history['val_accuracy'], label='Fine Tune DenseNet169 Accuracy')
    plt.legend()
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.grid()
    plt.subplot(212)
    plt.title('Test Loss')
    plt.plot(base_model_results.history['val_loss'], label='Base DenseNet169 Accuracy')
    plt.plot(finetune_model_results.history['val_loss'], label='Fine Tune DenseNet169 Accuracy')
    plt.legend()
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.grid()
    plt.show()


def call_preprocessing(data_directory, model_type):
    '''
    Create a PreProcceing object.
    :param:
    model_type - String of the model type, can be "base" or "finetune".
    :return:
    train_generator - Generator of the train dataset.
    valid_generator - Generator of the validation dataset.
    '''
    preprocess_object = PreProcessing(data_directory)
    train_generator = preprocess_object.train_generator(model_type)
    valid_generator = preprocess_object.valid_generator(model_type)
    return train_generator, valid_generator


def call_base_model(trained_model=None):
    '''
    Create or load a DenseNet169 base model.
    :param:
    trained_model - Directory to a saved model if given, if not then None.
    :return:
    base_model - Based DenseNet169 model.
    '''
    if trained_model:
        base_model = tf.keras.models.load_model(trained_model)
    else:
        base_model = DenseNet169_Base()
        base_model = base_model.create_model()
    return base_model


def call_fine_tuned_model(trained_model=None):
    '''
    Create or load a DenseNet169 finetuned model.
    :param:
    trained_model - Directory to a saved model if given, if not then None.
    :return:
    finetune_model - Finetuned DenseNet169 model.
    reduce_lr_callback -  Keras Callback that modify the learning rate when recognize a plateau on the validation loss metric.
    '''
    finetune_model = DenseNet169_FineTune()
    reduce_lr_callback = finetune_model.reduce_lr
    if trained_model:
        finetune_model = tf.keras.models.load_model(trained_model)
    else:
        finetune_model = finetune_model.create_model()
    return finetune_model, reduce_lr_callback


def train_model(model, train_generator, valid_generator, callbacks=None, num_epochs=10):
    '''
    Train the model.

    :param:
    model - Finetuned or base DenseNet169 model.
    train_generator - Generator of the train dataset.
    valid_generator - Generator of the validation dataset.
    callbacks - Keras Callback that modify the learning rate when recognize a plateau on the validation loss metric, if not given then None.
    num_epochs - Number of epochs the model is trained.
    :return:
    model - Trained base or finetuned DenseNet169 model.
    '''
    model.fit(x=train_generator, validation_data=valid_generator, epochs=num_epochs, callbacks=[callbacks], verbose=2)
    return model


def predict_and_evaluate_model(model, valid_generator):
    '''
    Predict and evaluate the loss and the accuracy of the labels within the validation set.

    :param:
    valid_generator - Generator of the validation dataset.
    :return:
    prediction - list of predicted labels.
    evaluation - Loss and Accuracy metrics for model prediction.
    '''
    prediction = model.predict(valid_generator, verbose=1)
    evaluation = model.evaluate(valid_generator, verbose=1)
    return prediction, evaluation
