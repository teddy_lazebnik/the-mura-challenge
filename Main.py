from Tasks import *

### Hard Coded data directory parameter ###
DATA_DIRECTORY = "C:\\Users\\alonav\\HIT_deep_learning\\Final Project\\MURA-v1.1"
TRAIN_MODEL = True
TRAINED_BASE_MODEL = None
TRAINED_FT_MODEL = "densenet169_finetune_model.h5"
###########################################


if __name__ == '__main__':
    finetune_model, reduce_lr_callback = call_fine_tuned_model(trained_model=TRAINED_FT_MODEL)
    ft_train_generator, ft_valid_generator = call_preprocessing(DATA_DIRECTORY, "finetune")
    base_model = call_base_model(trained_model=TRAINED_BASE_MODEL)
    base_train_generator, base_valid_generator = call_preprocessing("base")
    if TRAIN_MODEL:
        base_model = train_model(base_model, base_train_generator, base_valid_generator)
        base_model.save("densenet169_base_model")
        finetune_model = train_model(finetune_model, ft_train_generator, ft_valid_generator, reduce_lr_callback)
        finetune_model.save("densenet169_finetune_model")

    base_model_prediction = predict_and_evaluate_model(base_model, base_valid_generator)
    ft_model_prediction = predict_and_evaluate_model(finetune_model, ft_valid_generator)
    plot(base_model_prediction, ft_model_prediction)
