import tensorflow

device_name = tensorflow.test.gpu_device_name()
if device_name != '/device:GPU:0':
    import tensorflow as tf
else:
    with tensorflow.device('/device:GPU:0'):
        import tensorflow as tf


class DenseNet169_FineTune:
    def __init__(self):
        '''
        image_shape - modified image shape to improve model accuracy.
        DenseNet169 - Base DenseNet169 model, pre-trained on ImageNet dataset.
        reduce_lr - Keras Callback that modify the learning rate when recognize a plateau on the validation loss metric.
        '''
        self.image_shape = (320, 320, 3)
        self.densenet169 = tf.keras.applications.DenseNet169(weights='imagenet', include_top=False,
                                                             input_shape=self.image_shape, pooling='avg')
        self.reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=1, verbose=1)

    def create_model(self):
        '''
        Create a DenseNet169 model based on ImageNet dataset, with modified input layer to get images with shape
        (320,320,3) and modified last layers - Dense with Sigmoid activation function, to predict the probability of a class
        (1 or 0).
        Also, set a callback that modify the learning rate when recognize a plateau on the validation loss metric.
        :return:
        modelDenseNet169 - Finetuned DenseNet169 model.
        '''
        modeldensenet169 = tf.keras.models.Sequential(
            [self.densenet169, tf.keras.layers.Flatten(), tf.keras.layers.Dense(1, activation='sigmoid')])
        modeldensenet169.compile(loss=tf.keras.losses.binary_crossentropy,
                                 optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.9, beta_2=0.999),
                                 metrics=['accuracy'])
        return modeldensenet169
